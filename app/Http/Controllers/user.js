app.controller('userController', function($scope, $http, API_URL) {
	$http({
		method: 'GET',
		url: API_URL + "users"
	}).then(function(response){
		$scope.users = response.data.users;
		console.log(response);
	}, function(error){
		console.log(error);
		alert('Error has ocurred');
	});

	$scope.toggle = function(modalstate, id){
		$scope.modalstate = modalstate;
		$scope.user = null;
		
	}
})