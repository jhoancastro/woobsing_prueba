<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'error' => false,
            'user' => Users::all(),
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Validation = Validator::make($request->all(),[
            'name' => 'required',
            'lastname' => 'required',
            'numbre_phone' => 'required',
            'email' => 'required|email|unique:user,email',
            'adress' => 'required'
        ]);

        if ($Validation->fails()) {

            return response()->json([
                'error' => true,
                'user' => $Validation->errors(),
            ], 200);

        } else {
           
            $user = new Users;
            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->numbre_phone = $request->input('numbre_phone');
            $user->email = $request->input('email');
            $user->adress = $request->input('adress');
            $user->save();

            return response()->json([
                'error' => false,
                'user' => $user,
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Users::find($id);
        if (is_null($user)) {

            return response()->json([
                'error' => true,
                'message' =>'Not found ID',
            ], 404);

        } else {

             return response()->json([
                'error' => false,
                'user' => $user,
            ], 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Validation = Validator::make($request->all(),[
            'name' => 'required',
            'lastname' => 'required',
            'numbre_phone' => 'required',
            'email' => 'required|email|unique:user,email',
            'adress' => 'required'
        ]);

        if ($Validation->fails()) {

            return response()->json([
                'error' => true,
                'user' => $Validation->errors(),
            ], 200);

        } else {
           
            $user = Users::find($id);
            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->numbre_phone = $request->input('numbre_phone');
            $user->email = $request->input('email');
            $user->adress = $request->input('adress');
            $user->save();

            return response()->json([
                'error' => false,
                'user' => $user,
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Users::find($id);
        if (is_null($user)) {

            return response()->json([
                'error' => true,
                'message' =>'Not found user',
            ], 404);

        } else {
            $user->delete();
             return response()->json([
                'error' => false,
                'message' =>'User successfully delete',
            ], 200);
        }
    }
}
